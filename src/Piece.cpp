#include "include/Piece.hpp"
#include "include/Board.hpp"

using namespace std;
using namespace sf;
using namespace Chess;

Piece::Piece ()
{
    owner = Player::White;
}

Piece::~Piece ()
{
    delete sprite;
}

void Piece::SetSprite (Sprite* _sprite)
{
    sprite = _sprite;
}

void Piece::Move (Vector2i _position)
{
    position = _position;
}

void Piece::Draw (RenderWindow* window, bool isSelection)
{
    sprite->setPosition (getRawPosition ());

    if (isSelection)
    {
        sprite->setColor (Color::Red);
    }
    else
    {
        sprite->setColor (Color::White);
    }
    

    window->draw (*sprite);
}

Vector2f Piece::getRawPosition ()
{
    int pixelsPerSquare = (int) ((float) ResX / 8.0f);
    return Vector2f (pixelsPerSquare * position.x, ResY - (pixelsPerSquare * (position.y + 1)));
}

Vector2i Piece::GetPosition ()
{
    return position;
}

void Piece::SetOwner (Player _owner)
{
    owner = _owner;
}

Player Piece::GetOwner ()
{
    return owner;
}

void Piece::SetBoard (Board* board)
{
    this->board = board;
}