#include "include/Game.hpp"

using namespace std;
using namespace Chess;
using namespace sf;

Game* Game::gameInstance;

Game::Game ()
{
    initialized = false;
    turn = Player::White;
    selection = nullptr;

    turnAmount = 0;

    Game::gameInstance = this;
}

Game::~Game ()
{
    delete board;
    delete window;
}

void Game::Run ()
{
    if (! initialized)
    {
        cerr << "ERROR: instance was not initialized" << endl;
        return;
    }

    window = new RenderWindow (VideoMode (ResX, ResY, ColorDepth), WindowTitle);

    //window->setVerticalSyncEnabled (true);
    window->setFramerateLimit (MaxFramerate);

    while (window->isOpen ())
    {
        Event event;
        while (window->pollEvent (event))
        {
            if (event.type == Event::Closed)
            {
                window->close ();
            }
            if (event.type == Event::MouseButtonReleased)
            {
                if (event.mouseButton.button == Mouse::Left)
                {
                    Vector2f raw = Vector2f ((float) event.mouseButton.x, (float) event.mouseButton.y);
                    HandleSelection (raw);
                }
            }
        }

        window->clear ();

        board->Draw (window, player, selection);

        window->display ();        
    }

    NetworkCore::Disconnect ();
}

void Game::HandleSelection (Vector2f newPosition)
{

    Vector2i pos = Board::RawToGamePosition (newPosition);

    if (selection == nullptr)
    {
        Piece* _piece = board->GetPieceAt (pos);
        if (_piece == nullptr || _piece->GetOwner () != player)
        {
            selection = nullptr;
        }
        else
        {
            selection = _piece;
        }
    }
    else
    {
        Vector2i oldPosition = selection->GetPosition ();

        if (turn == player && board->MovePiece (selection, pos, player))
        {
            NetworkCore::CallMovePiece (oldPosition, pos);
            ChangeTurn ();
        }
        selection = nullptr;
    }
}

void Game::Initialize (string ip, int port)
{
    player = Player::White;
    board = new Board (player);
    NetworkCore::Initialize (board, this, ip, port);
    initialized = true;
}

void Game::Initialize (int port)
{
    player = Player::Black;
    board = new Board (player);
    NetworkCore::Initialize (board, this, port);
    initialized = true;
}

void Game::ChangeTurn ()
{
    if (turn == Player::Black)
    {
        turn = Player::White;
    }
    else
    {
        turn = Player::Black;
    }
    turnAmount++;
}

Player Game::GetSelfPlayer ()
{
    return player;
}

int Game::GetTurnAmount ()
{
    return turnAmount;
}

Game* Game::GetInstance ()
{
    if (Game::gameInstance == nullptr)
    {
        Game::gameInstance = new Game ();
    }
    return Game::gameInstance;
}