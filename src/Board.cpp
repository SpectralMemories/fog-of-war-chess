#include "include/Board.hpp"

using namespace std;
using namespace Chess;
using namespace sf;

Board::Board (Player player)
{
    Texture* texture = new Texture ();
    if (! texture->loadFromFile (BOARD_SPRITE_PATH))
    {
        cerr << "ERROR: COULD NOT LOAD " << BOARD_SPRITE_PATH << endl;
        return;
    }

    texture->setSmooth (false);

    sprite = new Sprite ();
    sprite->setTexture (*texture);

    Vector2f targetSize ((float) ResX, (float) ResY); 

    sprite->setScale (
        targetSize.x / sprite->getLocalBounds().width, 
        targetSize.y / sprite->getLocalBounds().height
    );

    Texture* FoWtexture = new Texture ();
    if (! FoWtexture->loadFromFile (FOGOFWAR_PATH))
    {
        cerr << "ERROR: COULD NOT LOAD " << FOGOFWAR_PATH << endl;
    }

    fogSprite = new Sprite ();
    fogSprite->setTexture (*FoWtexture);

    ourPlayer = player;

    PopulateBoard (player);
}

Board::~Board ()
{
    vector<Piece*>::iterator whiteIterator = whitePieces.begin ();
    vector<Piece*>::iterator blackIterator = blackPieces.begin ();

    while (whiteIterator != whitePieces.end ())
    {
        delete (*whiteIterator);
        whiteIterator++;
    }

    while (blackIterator != blackPieces.end ())
    {
        delete (*blackIterator);
        blackIterator++;
    }

    delete fogSprite;
    delete sprite;
}

void Board::Draw (RenderWindow* window, Player player, Piece* selectedPiece)
{
    window->draw (*sprite);

    vector<Piece*>::iterator whiteIterator = whitePieces.begin ();
    vector<Piece*>::iterator blackIterator = blackPieces.begin ();

    bool isSelected;

    while (whiteIterator != whitePieces.end ())
    {
        isSelected = ((*whiteIterator) == selectedPiece);

        if (ShouldDraw ((*whiteIterator), Player::White, player))
        {
            (*whiteIterator)->Draw (window, isSelected);
        }
        whiteIterator++;
    }

    while (blackIterator != blackPieces.end ())
    {
        isSelected = ((*blackIterator) == selectedPiece);

        if (ShouldDraw ((*blackIterator), Player::Black, player))
        {
            (*blackIterator)->Draw (window, isSelected);
        }
        blackIterator++;
    }

    DrawFogOfWar (window, player);
}

void Board::DrawFogOfWar (RenderWindow* window, Player side)
{
    vector<Piece*>::iterator viewer;
    vector<Piece*>::iterator end;
    vector<Piece*>::iterator begin;

    if (side == Player::White)
    {
        begin = whitePieces.begin ();
        end = whitePieces.end ();
    }
    else if (side == Player::Black)
    {
        begin = blackPieces.begin ();
        end = blackPieces.end ();
    }

    bool isVisible = false;
    for (int x = 0; x < 8; x++)
    {
        for (int y = 0; y < 8; y++)
        {
            viewer = begin;
            isVisible = false;
            while (viewer != end)
            {
                Vector2i piecePosition = (*viewer)->GetPosition ();
                Vector2i targetPosition = Vector2i (x, y);
                
                int xDistance = (int) abs (piecePosition.x - targetPosition.x);
                int yDistance = (int) abs (piecePosition.y - targetPosition.y);

                if (xDistance <= ViewDistance && yDistance <= ViewDistance)
                {
                    isVisible = true;
                    break;
                }

                viewer++;
            }
            if (! isVisible)
            {
                int step = (int) ((float)ResX / 8.0f) + 1;
                //Draw at (x,y)
                Vector2f currentPosition = Vector2f (x * step, ResY - ((y + 1) * step));
                fogSprite->setPosition (currentPosition);
                window->draw (*fogSprite);
            }
        }
    }
}

bool Board::ShouldDraw (Piece* piece, Player pieceOwner, Player side)
{
    if (pieceOwner == side)
    {
        return true; //Its our own piece
    }

    vector<Piece*>::iterator viewer;
    vector<Piece*>::iterator end;

    if (side == Player::White)
    {
        viewer = whitePieces.begin ();
        end = whitePieces.end ();
    }
    else if (side == Player::Black)
    {
        viewer = blackPieces.begin ();
        end = blackPieces.end ();
    }

    while (viewer != end)
    {
        Vector2i allyPosition = (*viewer)->GetPosition ();
        Vector2i targetPosition = piece->GetPosition ();

        int xDistance = (int) abs (allyPosition.x - targetPosition.x);
        int yDistance = (int) abs (allyPosition.y - targetPosition.y);

        if (xDistance <= ViewDistance && yDistance <= ViewDistance)
        {
            return true;
        }

        viewer++;
    }

    return false;
}

Piece* Board::GetPieceAt (Vector2i position)
{

    vector<Piece*>::iterator whiteIterator = whitePieces.begin ();

    while (whiteIterator != whitePieces.end ())
    {
        if ((*whiteIterator)->GetPosition () == position)
        {
            return *whiteIterator;
        }
        whiteIterator++;
    }
    
    vector<Piece*>::iterator blackIterator = blackPieces.begin ();

    while (blackIterator != blackPieces.end ())
    {
        if ((*blackIterator)->GetPosition () == position)
        {
            return *blackIterator;
        }
        blackIterator++;
    }

    return nullptr;
    
}

bool Board::MovePiece (Piece* piece, Vector2i newPosition, Player player)
{

    if (! piece->CanMove (newPosition))
    {
        return false;
    }
    
    Piece* pieceToDestroy = nullptr;

    Piece* pieceAtTarget = GetPieceAt (newPosition);

    if (pieceAtTarget != nullptr)
    {
        if (dynamic_cast<King*> (pieceAtTarget) != NULL)
        {
            //Its a king. We can't eat that
            return false;
        }
        if (pieceAtTarget->GetOwner () == player)
        {
            return false; //This is our own piece
        }
        else
        {
            pieceToDestroy = pieceAtTarget; //kill enemy piece
        }
        
    }

    if (IsKingThreatened (piece, newPosition, pieceToDestroy, player))
    {
        return false; //King would be threatened if we execute the move
    }

    if (pieceToDestroy != nullptr) DestroyPiece (pieceToDestroy);
    piece->Move (newPosition); //move self

    return true;
}

bool Board::IsKingThreatened (Piece* movedPiece, sf::Vector2i newPosition, Piece* deadPiece, Player player)
{
    vector<Piece*>* enemyPieces;
    if (player == Player::Black)
    {
        enemyPieces = &whitePieces;
    }
    else
    {
        enemyPieces = &blackPieces;
    }
    
    vector<Piece*>::iterator enemyIterator = enemyPieces->begin ();
    Vector2i oldPos;
    if (movedPiece != nullptr)
    {
        oldPos = movedPiece->GetPosition ();
        movedPiece->Move (newPosition);
    }

    while (enemyIterator != enemyPieces->end ())
    {
        if (*enemyIterator == deadPiece)
        {
            enemyIterator++;
            continue;
        }

        if ((*enemyIterator)->CanMove (king->GetPosition ()))
        {
            if (movedPiece != nullptr) movedPiece->Move (oldPos); //Reverse any changes before returning true
            return true;
        }

        if (movedPiece != nullptr) movedPiece->Move (oldPos); //Reverse any changes
        enemyIterator++;
    }

    return false;
}

void Board::MovePieceForce (Vector2i oldPosition, Vector2i newPosition)
{
    Piece* piece = GetPieceAt (oldPosition);
    Piece* pieceAtTarget = GetPieceAt (newPosition);

    if (piece != nullptr)
    {
        if (pieceAtTarget != nullptr)
        {
            DestroyPiece (pieceAtTarget);
        }
        piece->Move (newPosition);
    }
}

bool Board::DestroyPiece (Piece* piece)
{
    vector<Piece*>* pieceArray = nullptr;
    
    if (piece->GetOwner () == Player::White)
    {
        pieceArray = &whitePieces;
    }
    else
    {
        pieceArray = &blackPieces;
    }

    vector<Piece*>::iterator iter = pieceArray->begin ();

    while (iter != pieceArray->end ())
    {
        Piece* currentPiece = *(iter);
        
        if (currentPiece == piece)
        {
            pieceArray->erase (iter);
            return true;
        }
        iter++;
    }
    return false;
}

bool Board::DestroyPiece (Vector2i position)
{
    Piece* piece = GetPieceAt (position);
    
    if (piece != nullptr)
    {
        return DestroyPiece (piece);
    }

    return false;
}

void Board::PopulateBoard (Player player)
{
    //White Pawns
    for (int i = 0; i < 8; i++)
    {
        Pawn* current = new Pawn ();
        current->Move (Vector2i (i, 1));

        //Yeah I know, I could use the same texture for multiple pieces, but screw it for now
        Texture* currentTexture = new Texture ();
        if (! currentTexture->loadFromFile (WHITEPAWN_PATH))
        {
            cerr << "ERROR: COULD NOT LOAD " << WHITEPAWN_PATH << endl;
            return;
        }

        Sprite* currentSprite = new Sprite ();
        currentSprite->setScale ( Vector2f (0.3f, 0.3f));
        currentSprite->setTexture (*currentTexture);

        current->SetSprite (currentSprite);
        current->SetBoard (this);

        whitePieces.push_back (current);
    }
    //Black Pawns
    for (int i = 0; i < 8; i++)
    {
        Pawn* current = new Pawn ();
        current->Move (Vector2i (i, 6));
        current->SetOwner (Player::Black);

        //Yeah I know, I could use the same texture for multiple pieces, but screw it for now
        Texture* currentTexture = new Texture ();
        if (! currentTexture->loadFromFile (BLACKPAWN_PATH))
        {
            cerr << "ERROR: COULD NOT LOAD " << BLACKPAWN_PATH << endl;
            return;
        }

        Sprite* currentSprite = new Sprite ();
        currentSprite->setScale ( Vector2f (0.3f, 0.3f));
        currentSprite->setTexture (*currentTexture);

        current->SetSprite (currentSprite);
        current->SetBoard (this);

        blackPieces.push_back (current);
    }

    //White Bishops
    for (int i = 0; i < 2; i++)
    {
        Bishop* current = new Bishop ();
        current->Move (Vector2i (2 + ( 3 * i), 0));

        Texture* currentTexture = new Texture ();
        if (! currentTexture->loadFromFile (WHITEBISHOP_PATH))
        {
            cerr << "ERROR: COULD NOT LOAD " << WHITEBISHOP_PATH << endl;
            return;
        }

        Sprite* currentSprite = new Sprite ();
        currentSprite->setScale ( Vector2f (0.3f, 0.3f));
        currentSprite->setTexture (*currentTexture);

        current->SetSprite (currentSprite);
        current->SetBoard (this);

        whitePieces.push_back (current);
    }

    //Black Bishops
    for (int i = 0; i < 2; i++)
    {
        Bishop* current = new Bishop ();
        current->Move (Vector2i (2 + ( 3 * i), 7));
        current->SetOwner (Player::Black);

        Texture* currentTexture = new Texture ();
        if (! currentTexture->loadFromFile (BLACKBISHOP_PATH))
        {
            cerr << "ERROR: COULD NOT LOAD " << BLACKBISHOP_PATH << endl;
            return;
        }

        Sprite* currentSprite = new Sprite ();
        currentSprite->setScale ( Vector2f (0.3f, 0.3f));
        currentSprite->setTexture (*currentTexture);

        current->SetSprite (currentSprite);
        current->SetBoard (this);

        blackPieces.push_back (current);
    }

    //White Tower
    for (int i = 0; i < 2; i++)
    {
        Tower* current = new Tower ();
        current->Move (Vector2i (7 * i, 0));

        Texture* currentTexture = new Texture ();
        if (! currentTexture->loadFromFile (WHITETOWER_PATH))
        {
            cerr << "ERROR: COULD NOT LOAD " << WHITETOWER_PATH << endl;
            return;
        }

        Sprite* currentSprite = new Sprite ();
        currentSprite->setScale ( Vector2f (0.3f, 0.3f));
        currentSprite->setTexture (*currentTexture);

        current->SetSprite (currentSprite);
        current->SetBoard (this);

        whitePieces.push_back (current);
    }

    //Black Tower
    for (int i = 0; i < 2; i++)
    {
        Tower* current = new Tower ();
        current->Move (Vector2i (7 * i, 7));
        current->SetOwner (Player::Black);

        Texture* currentTexture = new Texture ();
        if (! currentTexture->loadFromFile (BLACKTOWER_PATH))
        {
            cerr << "ERROR: COULD NOT LOAD " << BLACKTOWER_PATH << endl;
            return;
        }

        Sprite* currentSprite = new Sprite ();
        currentSprite->setScale ( Vector2f (0.3f, 0.3f));
        currentSprite->setTexture (*currentTexture);

        current->SetSprite (currentSprite);
        current->SetBoard (this);

        blackPieces.push_back (current);
    }

    //White Knight
    for (int i = 0; i < 2; i++)
    {
        Knight* current = new Knight ();
        current->Move (Vector2i (1 + (5 * i), 0));

        Texture* currentTexture = new Texture ();
        if (! currentTexture->loadFromFile (WHITEKNIGHT_PATH))
        {
            cerr << "ERROR: COULD NOT LOAD " << WHITEKNIGHT_PATH << endl;
            return;
        }

        Sprite* currentSprite = new Sprite ();
        currentSprite->setScale ( Vector2f (0.3f, 0.3f));
        currentSprite->setTexture (*currentTexture);

        current->SetSprite (currentSprite);
        current->SetBoard (this);

        whitePieces.push_back (current);
    }

    //Black Knight
    for (int i = 0; i < 2; i++)
    {
        Knight* current = new Knight ();
        current->Move (Vector2i (1 + (5 * i), 7));
        current->SetOwner (Player::Black);

        Texture* currentTexture = new Texture ();
        if (! currentTexture->loadFromFile (BLACKKNIGHT_PATH))
        {
            cerr << "ERROR: COULD NOT LOAD " << BLACKKNIGHT_PATH << endl;
            return;
        }

        Sprite* currentSprite = new Sprite ();
        currentSprite->setScale ( Vector2f (0.3f, 0.3f));
        currentSprite->setTexture (*currentTexture);

        current->SetSprite (currentSprite);
        current->SetBoard (this);

        blackPieces.push_back (current);
    }

    //White King
    {
        King* current = new King ();
        current->Move (Vector2i (3, 0));

        Texture* currentTexture = new Texture ();
        if (! currentTexture->loadFromFile (WHITEKING_PATH))
        {
            cerr << "ERROR: COULD NOT LOAD " << WHITEKING_PATH << endl;
            return;
        }

        Sprite* currentSprite = new Sprite ();
        currentSprite->setScale ( Vector2f (0.3f, 0.3f));
        currentSprite->setTexture (*currentTexture);

        current->SetSprite (currentSprite);
        current->SetBoard (this);

        if (player == Player::White) king = current;
        whitePieces.push_back (current);
        
    }

    //Black King
    {
        King* current = new King ();
        current->Move (Vector2i (3, 7));
        current->SetOwner (Player::Black);

        Texture* currentTexture = new Texture ();
        if (! currentTexture->loadFromFile (BLACKKING_PATH))
        {
            cerr << "ERROR: COULD NOT LOAD " << BLACKKING_PATH << endl;
            return;
        }

        Sprite* currentSprite = new Sprite ();
        currentSprite->setScale ( Vector2f (0.3f, 0.3f));
        currentSprite->setTexture (*currentTexture);

        current->SetSprite (currentSprite);
        current->SetBoard (this);

        if (player == Player::Black) king = current;
        blackPieces.push_back (current);
    }

    //White Queen
    {
        Queen* current = new Queen ();
        current->Move (Vector2i (4, 0));

        Texture* currentTexture = new Texture ();
        if (! currentTexture->loadFromFile (WHITEQUEEN_PATH))
        {
            cerr << "ERROR: COULD NOT LOAD " << WHITEQUEEN_PATH << endl;
            return;
        }

        Sprite* currentSprite = new Sprite ();
        currentSprite->setScale ( Vector2f (0.3f, 0.3f));
        currentSprite->setTexture (*currentTexture);

        current->SetSprite (currentSprite);
        current->SetBoard (this);

        whitePieces.push_back (current);
    }

    //Black Queen
    {
        Queen* current = new Queen ();
        current->Move (Vector2i (4, 7));
        current->SetOwner (Player::Black);

        Texture* currentTexture = new Texture ();
        if (! currentTexture->loadFromFile (BLACKQUEEN_PATH))
        {
            cerr << "ERROR: COULD NOT LOAD " << BLACKQUEEN_PATH << endl;
            return;
        }

        Sprite* currentSprite = new Sprite ();
        currentSprite->setScale ( Vector2f (0.3f, 0.3f));
        currentSprite->setTexture (*currentTexture);

        current->SetSprite (currentSprite);
        current->SetBoard (this);

        blackPieces.push_back (current);
    }
}

Vector2i Board::RawToGamePosition (Vector2f position)
{
    Vector2i returnPosition = Vector2i (0, 0);

    int caseSize = (int) ((float) ResX / 8.0f);

    returnPosition.x = (int) ((float) position.x / (float) caseSize);

    returnPosition.y = (8 - 1) - ((int) ((float) position.y / (float) caseSize));

    return returnPosition;
}