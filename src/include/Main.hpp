#pragma once

//We need those for now
#include <string>
#include <iostream>

//We need to start the game
#include "Game.hpp"

namespace Chess
{
    //Start point
    int main (int argc, char** argv);
}