#pragma once

#include <SFML/Graphics.hpp>
#include "Definitions.hpp"
#include "Player.hpp"

namespace Chess
{
    //See .cpp file for the #include
    class Board; //Forward declaration

    //Abstract. This class represents a piece
    class Piece
    {
        private:
            //Returns the raw position of the piece based on resolution
            sf::Vector2f getRawPosition ();
        protected:
            //Link to the parent board
            Board* board;
            //Owner of the piece
            Player owner;
            //The graphical representation of the piece
            sf::Sprite* sprite;
            //The position of the piece on the board
            sf::Vector2i position;
            Piece (void);

            //TODO: Add a complete constructor
        public:
            ~Piece (void);

            //Draws the single piece on the screen
            void Draw (sf::RenderWindow* window, bool isSelection = false);
            
            //Sets the sprite of the piece
            void SetSprite (sf::Sprite* _sprite);
            //Set the board link
            void SetBoard (Board* board);
            //Manually move the piece. Use Board::MovePiece for legality check
            void Move (sf::Vector2i _position);
            //Sets the owner of the piece
            void SetOwner (Player _owner);

            //Returns the position of the piece on the board
            sf::Vector2i GetPosition (void);
            //Returns the owner of the piece
            Player GetOwner (void);

            //Abstract. Will be overriden by child classes
            virtual bool CanMove (sf::Vector2i position) = 0;
    };
}