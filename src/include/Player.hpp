#pragma once

namespace Chess
{
    //Represents the two available sides
    enum Player
    {
        White,
        Black
    };
}