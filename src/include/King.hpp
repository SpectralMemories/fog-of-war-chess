#pragma once

#include <SFML/Graphics.hpp>

#include "Piece.hpp"

namespace Chess
{
    //This class represent the king
    class King final : public Piece
    {
        private:

        public:
            King (void);
            ~King (void);
            
            //Can we move there?
            bool CanMove (sf::Vector2i position) override;
    };
}