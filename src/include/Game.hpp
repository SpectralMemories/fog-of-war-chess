#pragma once

#include <iostream> //for debug purposes
//We need that
#include <string>

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>

#include "Board.hpp"
#include "NetworkCore.hpp"
#include "Player.hpp"

#include "Definitions.hpp"

#define ColorDepth 32
#define MaxFramerate 60

#define WindowTitle "Fog of War Chess"


namespace Chess
{
    //The Game class itself. It controls the flow and the end of the game. Singleton
    class Game final
    {
        private:
            Game (void); //MIGHT CAUSE A PROBLEM. IF IT DOES, MOVE TO PUBLIC

            //This is the only allowed instance
            static Game* gameInstance;

            //The window in which we render the image
            sf::RenderWindow* window;
            //Are we ready to play?
            bool initialized;
            //The board
            Board* board;
            //The current turn
            Player turn;
            //Our current selection. Can be null
            Piece* selection;
            //Our color
            Player player;
            //Is the game currently running. Differs from initialized
            bool running;
            //How many turns have past
            int turnAmount;

            //This will properly set the selection var based on mouse input
            void HandleSelection (sf::Vector2f rawPosition);
            //Handled the game over based on the winner of the game
            void ShowGameover (Player winner);

        public:

            //Returns the game instance. Creates one if none exists
            static Game* GetInstance (void);

            
            ~Game (void);


            //Return the amound of turn that have past
            int GetTurnAmount (void);
            //Return our own player
            Player GetSelfPlayer (void);

            //Starts the game. It must be initialized first
            void Run (void);
            //Initializes the game as a client (black)
            void Initialize (std::string ip, int port);
            //Initializes the game as a server (white)
            void Initialize (int port);
            //Handles a change of turn
            void ChangeTurn (void);
    };
}