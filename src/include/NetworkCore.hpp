#pragma once

//We need those
#include <SFML/Network.hpp>
#include <string>
#include <iostream>
#include <thread> //For multithreading
#include <sstream>

//We want to wait
#include <unistd.h> //This C header has C++ support

//We shall move pieces on the board
#include "Piece.hpp"

//Size of the buffer reserved for remote I/O
#define bufferSize 200

//Accepted commands
#define MOVE_COMMAND "move"
#define TRANSFORM_COMMAND "transform"
#define SURRENDER_COMMAND "surrender"


namespace Chess
{
    //See the .cpp file for the actual #include
    class Board; //Forward declaration because Board includes this class
    class Game; //Forward declaration because Game includes this class

    //This static class handles the network communication
    class NetworkCore final
    {
        private:
            NetworkCore (void);
            ~NetworkCore (void);

            //Halting. This method will listen and act according to incoming commands
            //From the remote player
            static void ListenAndAct (void);

            //Our connection to the other player
            static sf::TcpSocket* inputSocket;
            //Are we connected?
            static bool connected;
            //Link to the board
            static Board* board;
            //Link to the game
            static Game* game;
            //Handle a move command
            static void HandleMove (std::string);

            //Splits a string into multiple pieces
            static std::vector<std::string> Split (const std::string& str, const std::string& delim);

        public:
            //Initializes the NetworkCode as a client
            static void Initialize (Board* _board, Game* _game, const std::string ip, const int port);
            //Initializes the NetworkCore as a server
            static void Initialize (Board* _board, Game* _game, const int port);

            //Asks the remote to move a piece
            static void CallMovePiece (Piece* piece, sf::Vector2i newPosition);
            static void CallMovePiece (sf::Vector2i oldPosition, sf::Vector2i newPosition);

            //Breaks the connection to the remote
            static void Disconnect (void);
    };
}