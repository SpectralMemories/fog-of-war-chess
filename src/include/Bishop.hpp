#pragma once

#include <SFML/Graphics.hpp>

#include "Piece.hpp"

namespace Chess
{
    //This class represents a bishop
    class Bishop final : public Piece
    {
        private:

        public:
            Bishop (void);
            ~Bishop (void);

            //Can this piece move at position?
            bool CanMove (sf::Vector2i position) override;
    };
}