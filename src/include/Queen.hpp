#pragma once

#include <SFML/Graphics.hpp>

#include "Piece.hpp"

namespace Chess
{
    //Represents the Queen
    class Queen final : public Piece
    {
        private:

        public:
            Queen (void);
            ~Queen (void);

            //Can we move there?
            bool CanMove (sf::Vector2i position) override;
    };
}