#pragma once

#include <SFML/Graphics.hpp>

#include "Piece.hpp"

namespace Chess
{
    //This class represents the knight
    class Knight final : public Piece
    {
        private:

        public:
            Knight (void);
            ~Knight (void);

            //Can we move there?
            bool CanMove (sf::Vector2i position) override;
    };
}