#pragma once

#include <SFML/Graphics.hpp>

#include "Piece.hpp"

namespace Chess
{
    //Represents the tower
    class Tower final : public Piece
    {
        private:

        public:
            Tower (void);
            ~Tower (void);
            
            //Can we move there?
            bool CanMove (sf::Vector2i position) override;
    };
}