#pragma once

///We need those for calculations
#include <vector>
#include <iostream>
#include <cmath>

//Pieces
#include "Piece.hpp"

#include "Pawn.hpp"
#include "King.hpp"
#include "Queen.hpp"
#include "Tower.hpp"
#include "Bishop.hpp"
#include "Knight.hpp"

//Board has to communicate to opponent
#include "NetworkCore.hpp"
#include "Player.hpp"

#include "Definitions.hpp"

//Sprite paths
#define BOARD_SPRITE_PATH "assets/bgs/board.png"

#define BLACKPAWN_PATH "assets/sprites/black_pawn.png"
#define BLACKKING_PATH "assets/sprites/black_king.png"
#define BLACKQUEEN_PATH "assets/sprites/black_queen.png"
#define BLACKTOWER_PATH "assets/sprites/black_tower.png"
#define BLACKBISHOP_PATH "assets/sprites/black_bishop.png"
#define BLACKKNIGHT_PATH "assets/sprites/black_knight.png"

#define WHITEPAWN_PATH "assets/sprites/white_pawn.png"
#define WHITEKING_PATH "assets/sprites/white_king.png"
#define WHITEQUEEN_PATH "assets/sprites/white_queen.png"
#define WHITETOWER_PATH "assets/sprites/white_tower.png"
#define WHITEBISHOP_PATH "assets/sprites/white_bishop.png"
#define WHITEKNIGHT_PATH "assets/sprites/white_knight.png"

#define FOGOFWAR_PATH "assets/sprites/fog.png"

namespace Chess
{
    ///This class represents our board
    class Board final
    {
        private:
            //All the pieces on the board at a moment
            std::vector<Piece*> whitePieces;
            std::vector<Piece*> blackPieces;

            //The sprite for the fog
            sf::Sprite* fogSprite;

            Player ourPlayer;

            //Our own king, the head of the board
            Piece* king;

            //Fills the board with the standart set of 32 pieces
            void PopulateBoard (Player player);

            //Destroys a piece if possible
            bool DestroyPiece (Piece* piece);
            bool DestroyPiece (sf::Vector2i position);

            //The look of the board itself
            sf::Sprite* sprite;

            //Should we draw a particular piece for a particular player
            bool ShouldDraw (Piece* piece, Player pieceOwner, Player side);
            //Draw the fog of war for a player
            void DrawFogOfWar (sf::RenderWindow* window, Player side);

        public:
            Board (Player player);
            ~Board (void);
            //Prepares the board for a new turn (misc. checkups)
            void PrepareForNewTurn (void);
            //Checks if the king is would be threatened in a configuration
            bool IsKingThreatened (Piece* movedPiece, sf::Vector2i newPosition, Piece* deadPiece, Player player);
            //Draw the actual board along with pieces and FOW
            void Draw (sf::RenderWindow* window, Player player, Piece* selectedPiece);
            //Returns the piece at a position. Null if there is none
            Piece* GetPieceAt (sf::Vector2i position);
            //Converts raw pixel position to a local board position based on resolution
            static sf::Vector2i RawToGamePosition (sf::Vector2f position);
            //Tries to move a piece. Will return false if its forbidden
            bool MovePiece (Piece* piece, sf::Vector2i newPosition, Player player);
            //Forcefully moves a piece regardless of legality
            void MovePieceForce (sf::Vector2i oldPosition, sf::Vector2i newPosition);
    };
}