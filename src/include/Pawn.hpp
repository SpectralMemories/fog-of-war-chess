#pragma once

#include <SFML/Graphics.hpp>
#include "Piece.hpp"
#include "Game.hpp"


namespace Chess
{
    //This class represents a pawn
    class Pawn final : public Piece
    {
        private:
            bool moved;
        public:
            Pawn (void);
            ~Pawn (void);
            
            //Can we move there?
            bool CanMove (sf::Vector2i position) override;
    };
}