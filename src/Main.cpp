#include "include/Main.hpp"

using namespace std;
using namespace Chess;

int main (int argc, char** argv)
{
    Game* game = Game::GetInstance ();
    string ip;
    int port;

    if(argc == 1)
    {
        cout << "Please enter an IP (if client) and the port" << endl;
        return EXIT_SUCCESS;
    }
    else if (argc == 2)
    {
       port = stoi ((string) *(argv + 1));
       game->Initialize (port);
    }
    else if (argc == 3)
    {
        ip = (string) *(argv + 1);
        port = stoi ((string) *(argv + 2));
        game->Initialize (ip, port);
    }
    else
    {
        cout << "Please enter an IP (if client) and the port" << endl;\
        return EXIT_SUCCESS;
    }

    game->Run ();

    delete game;
    
    return EXIT_SUCCESS;
}