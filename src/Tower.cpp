#include "include/Tower.hpp"
#include "include/Board.hpp"

using namespace std;
using namespace sf;
using namespace Chess;

Tower::Tower ()
{

}
Tower::~Tower ()
{

}


bool Tower::CanMove (sf::Vector2i position)
{
    int diffX = position.x - this->position.x;
    int diffY = position.y - this->position.y;

    

    if (! (diffX == 0 || diffY == 0))
    {
        return false;
    }
    
    Vector2i trajectory;
    if (diffX > 0) trajectory.x = 1;
    if (diffX < 0) trajectory.x = -1;

    if (diffY > 0) trajectory.y = 1;
    if (diffY < 0) trajectory.y = -1;

    Vector2i scanPosition = this->position;
    do //Scan for pieces blocking the way
    {
        scanPosition += trajectory;
        Piece* encounteredPiece = board->GetPieceAt (scanPosition);
        if (encounteredPiece != nullptr)
        {
            if (scanPosition == position) return true;
            return false;
        }
    } while (scanPosition != position);

    return true;
}