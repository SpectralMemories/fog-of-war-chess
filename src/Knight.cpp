#include "include/Knight.hpp"

using namespace std;
using namespace sf;
using namespace Chess;

Knight::Knight ()
{

}
Knight::~Knight ()
{

}


bool Knight::CanMove (sf::Vector2i position)
{
    Vector2i allowedDiffs[] =
    {
        Vector2i (1, 2),
        Vector2i (2, 1)
    };

    int diffX = abs (position.x - this->position.x);
    int diffY = abs (position.y - this->position.y);

    for (int i = 0; i < 2; i++)
    {
        if (diffX == allowedDiffs[i].x && diffY == allowedDiffs[i].y)
        {
            return true;
        }
    }

    
    return false;
}