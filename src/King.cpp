#include "include/King.hpp"

using namespace std;
using namespace sf;
using namespace Chess;

King::King ()
{

}
King::~King ()
{

}


bool King::CanMove (sf::Vector2i position)
{
    int diffX = position.x - this->position.x;
    int diffY = position.y - this->position.y;

    int totalDiff = abs (diffX) + abs (diffY);

    int acceptableDiff;

    if (diffX != 0 && diffY != 0)
    {
        acceptableDiff = 2; //We allow diagonal movements
    }
    else
    {
        acceptableDiff = 1;
    }
    
    return totalDiff <= acceptableDiff;
}