#include "include/NetworkCore.hpp"
#include "include/Board.hpp" //Complement NetworkCore.hpp's forward declaration
#include "include/Game.hpp" //Complement NetworkCore.hpp's forward declaration

using namespace std;
using namespace Chess;
using namespace sf;

//Forward declaration of private static members
TcpSocket* NetworkCore::inputSocket;
bool NetworkCore::connected;
Board* NetworkCore::board;
Game* NetworkCore::game;
//

NetworkCore::NetworkCore ()
{
    NetworkCore::connected = false;
}
NetworkCore::~NetworkCore ()
{
    Disconnect ();

    delete inputSocket;
}

void NetworkCore::Initialize (Board* _board, Game* _game, const string ip, const int port)
{
    cout << "Acting as a client!" << endl; //DEBUG
    cout << "Connecting to " << ip << " on port " << port << endl;

    TcpSocket* socket = new TcpSocket ();
    Socket::Status status = socket->connect ((IpAddress) ip, port);
    if(status != Socket::Done)
    {
        cout << "A connection error occured" << endl;
        return;
    }
    socket->setBlocking (true);
    NetworkCore::inputSocket = socket;
    NetworkCore::board = _board;
    NetworkCore::game = _game;
    

    cout << "Connection successful!" << endl; //DEBUG
    NetworkCore::connected = true;

    {
        thread workerThread (NetworkCore::ListenAndAct);
        workerThread.detach ();
    }
    cout << "Started listener thread" << endl; //DEBUG
}
void NetworkCore::Initialize (Board* _board, Game* _game, const int port)
{
    cout << "Acting as a server!" << endl; //DEBUG
    cout << "Hosting on port " << port << endl;

    TcpListener listener;

    if (listener.listen (port) != Socket::Done)
    {
        cout << "A connection error occured" << endl;
        return;
    }

    TcpSocket* newClient = new TcpSocket ();

    if (listener.accept (*newClient) != Socket::Done)
    {
        cout << "A connection error occured" << endl;
        return;
    }
    NetworkCore::connected = true;

    newClient->setBlocking (true);
    NetworkCore::inputSocket = newClient;
    NetworkCore::board = _board;
    NetworkCore::game = _game;

    cout << "Connection successful!" << endl; //DEBUG
    {
        thread workerThread (NetworkCore::ListenAndAct);
        workerThread.detach ();
    }
    cout << "Started listener thread" << endl; //DEBUG
}

void NetworkCore::CallMovePiece (Piece* piece, Vector2i newPosition)
{
    CallMovePiece (piece->GetPosition (), newPosition);
}

void NetworkCore::CallMovePiece (Vector2i oldPosition, Vector2i newPosition)
{
    ostringstream builtString;

    builtString << "move " << oldPosition.x << " " << oldPosition.y << " " << newPosition.x << " " << newPosition.y;
    string call = builtString.str ();

    char buffer [bufferSize];

    for (int i = 0; i < call.length (); i++)
    {
        buffer [i] = call [i];
    }

    inputSocket->send (buffer, bufferSize);
}

void NetworkCore::Disconnect ()
{
    if (! NetworkCore::connected)
    {
        return;
    }
    NetworkCore::inputSocket->disconnect ();
    NetworkCore::connected = false;
}

void NetworkCore::ListenAndAct ()
{
    char buffer [bufferSize];
    size_t received;

    while (NetworkCore::connected)
    {
        Socket::Status status;
        status = NetworkCore::inputSocket->receive (buffer, bufferSize, received);
        if (status != Socket::Done)
        {
            cout << "Error while receiving data from " << NetworkCore::inputSocket->getRemoteAddress () << ":" << NetworkCore::inputSocket->getLocalPort () << endl;
            usleep (2000000); //Wait two second before trying again 
            continue;
        }
        
        cout << "Received \"" << buffer << "\" from active connection" << endl; //DEBUG
        
        string message (buffer);

        if (message.rfind (MOVE_COMMAND, 0) == 0)
        {
            HandleMove (message);
        }
        if (message.rfind (TRANSFORM_COMMAND, 0) == 0)
        {
            
        }
        if (message.rfind (SURRENDER_COMMAND, 0) == 0)
        {
            
        }
        
        usleep (16666); //this ensures that the game wont eat all the CPU time if things go wrong (ex: socket disconnects)
    }
}


void NetworkCore::HandleMove (string message)
{
    vector<string> tokens = Split (message, " ");
    vector<string>::iterator tokenIter = tokens.begin ();

    Vector2i oldPos;
    Vector2i newPos;

    tokenIter++; //Skip word "move"

    oldPos.x = stoi (*tokenIter);
    tokenIter++;
    oldPos.y = stoi (*tokenIter);
    tokenIter++;

    newPos.x = stoi (*tokenIter);
    tokenIter++;
    newPos.y = stoi (*tokenIter);

    board->MovePieceForce (oldPos, newPos);
    game->ChangeTurn ();
}

vector<string> NetworkCore::Split(const string& str, const string& delim)
{
    vector<string> tokens;
    size_t prev = 0, pos = 0;
    do
    {
        pos = str.find(delim, prev);
        if (pos == string::npos) pos = str.length();
        string token = str.substr(prev, pos-prev);
        if (!token.empty()) tokens.push_back(token);
        prev = pos + delim.length();
    }
    while (pos < str.length() && prev < str.length());
    return tokens;
}
