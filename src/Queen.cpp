#include "include/Queen.hpp"
#include "include/Board.hpp"

using namespace std;
using namespace sf;
using namespace Chess;

Queen::Queen ()
{

}
Queen::~Queen ()
{

}


bool Queen::CanMove (sf::Vector2i position)
{
    int diffX =  (position.x - this->position.x);
    int diffY =  (position.y - this->position.y);

    bool legal = false;
    if (abs (diffX) == abs (diffY)) legal = true;
    if (diffX == 0 || diffY == 0) legal = true;

    if (!legal) return false; //move is not even legal

    Vector2i trajectory = Vector2i (0, 0);
    if (diffX > 0) trajectory.x = 1;
    if (diffX < 0) trajectory.x = -1;

    if (diffY > 0) trajectory.y = 1;
    if (diffY < 0) trajectory.y = -1;

    cout << "Target: " << position.x << "," << position.y << endl;
    cout << "Self: " << this->position.x << "," << this->position.y << endl;

    cout << "Diff: " << diffX << "," << diffY << endl;
    cout << "Trajectory: " << trajectory.x << "," << trajectory.y << endl;


    Vector2i scanPosition = this->position;
    do //Scan for pieces blocking the way
    {
        scanPosition += trajectory;
        Piece* encounteredPiece = board->GetPieceAt (scanPosition);
        if (encounteredPiece != nullptr)
        {
            if (scanPosition == position) return true;
            return false;
        }
    } while (scanPosition != position);

    return true;

}