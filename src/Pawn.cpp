#include "include/Pawn.hpp"

using namespace std;
using namespace sf;
using namespace Chess;

Pawn::Pawn (): Piece ()
{
    moved = false;
}

Pawn::~Pawn ()
{

}

bool Pawn::CanMove (sf::Vector2i position)
{
    Piece* pieceAtTarget = board->GetPieceAt (position);

    int acceptableRangeY = owner == Player::White ? 1 : -1;


    int diffX = position.x - this->position.x;
    int diffY = position.y - this->position.y;

    if (pieceAtTarget == nullptr)
    {
        if (diffY == acceptableRangeY || (!moved && diffY == acceptableRangeY * 2))
        {
            if (diffX == 0)
            {
                moved = true;
                return true;
            }
        }
    }
    else
    {
        if ((diffY == acceptableRangeY) && (abs (diffX) == 1))
        {
            moved = true;
            return true;
        }
    }
    return false; //Somehow we got there?
}

