# Fog of War Chess
This is a temporary name, a real name will be chosen later on

# Dependencies
### SFML
Any modern implementation of SFML will work. I tested with 2.5.1, so try it if you run into trouble

### C++ Standart Library (stdc++)
This should come with your OS. If not, uhhh... You got other problems to fix. Tested with g++ 8.2.1's libstdc++

# Compiling
Simply use the Makefile. Tested on Arch Linux, works fine with g++ 8.2.1

# Binary Releases
Will come later, for both Linux (Deb and PKGBUILD) and Windows (7+)
