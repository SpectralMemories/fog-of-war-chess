MF_DIR=$(dir $(realpath $(firstword $(MAKEFILE_LIST))))

include $(MF_DIR)mkfile_config.txt

all: chess

chess: $(BUILD_OBJECTS) $(SOURCE_DIR)Main.cpp
	$(CC) $(EXEC_FLAG) $(MISC_FLAGS) -o $(BIN_DIR)chess $(SOURCE_DIR)Main.cpp $(BUILD_OBJECTS) $(CFLAGS) $(LIBS)

game.o: $(SOURCE_DIR)Game.cpp
	$(CC) -c $(SOURCE_DIR)Game.cpp -o game.o

networkcore.o: $(SOURCE_DIR)NetworkCore.cpp
	$(CC) -c $(SOURCE_DIR)NetworkCore.cpp -o networkcore.o

board.o: $(SOURCE_DIR)Board.cpp
	$(CC) -c $(SOURCE_DIR)Board.cpp -o board.o

piece.o: $(SOURCE_DIR)Piece.cpp
	$(CC) -c $(SOURCE_DIR)Piece.cpp -o piece.o

pawn.o: $(SOURCE_DIR)Pawn.cpp
	$(CC) -c $(SOURCE_DIR)Pawn.cpp -o pawn.o

queen.o: $(SOURCE_DIR)Queen.cpp
	$(CC) -c $(SOURCE_DIR)Queen.cpp -o queen.o

king.o: $(SOURCE_DIR)King.cpp
	$(CC) -c $(SOURCE_DIR)King.cpp -o king.o

tower.o: $(SOURCE_DIR)Tower.cpp
	$(CC) -c $(SOURCE_DIR)Tower.cpp -o tower.o

bishop.o : $(SOURCE_DIR)Bishop.cpp
	$(CC) -c $(SOURCE_DIR)Bishop.cpp -o bishop.o

knight.o : $(SOURCE_DIR)Knight.cpp
	$(CC) -c $(SOURCE_DIR)Knight.cpp -o knight.o


clean:
	@rm -Rv $(BUILD_OBJECTS)

mrproper: clean
	@rm -v $(BIN_DIR)chess

install: chess

uninstall: